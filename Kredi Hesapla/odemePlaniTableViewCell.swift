//
//  odemePlaniTableViewCell.swift
//  Kredi Hesapla
//
//  Created by Atakan Cengiz KURT on 9.08.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class odemePlaniTableViewCell: UITableViewCell {

    @IBOutlet weak var ay: UILabel!
    @IBOutlet weak var taksit: UILabel!
    @IBOutlet weak var anaPara: UILabel!
    @IBOutlet weak var faiz: UILabel!
    @IBOutlet weak var kalan: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
