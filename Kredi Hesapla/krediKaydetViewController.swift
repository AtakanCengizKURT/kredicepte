//
//  krediKaydetViewController.swift
//  Kredi Hesapla
//
//  Created by Atakan Cengiz KURT on 14.08.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

func userDefaultsKayit(){
    UserDefaults.standard.set(krediTuruKayitli, forKey: "krediTuruKayitli")
    UserDefaults.standard.set(krediTutariKayitli, forKey: "krediTutariKayitli")
    UserDefaults.standard.set(faizOraniKayitli, forKey: "faizOraniKayitli")
    UserDefaults.standard.set(vadeKayitli, forKey: "vadeKayitli")
    UserDefaults.standard.set(taksitTutariKayitli, forKey: "taksitTutariKayitli")
    UserDefaults.standard.set(toplamOdemeKayitli, forKey: "toplamOdemeKayitli")
    UserDefaults.standard.set(toplamFaizKayitli, forKey: "toplamFaizKayitli")
    UserDefaults.standard.set(bankaIsmiKayitli, forKey: "bankaIsmiKayitli")
    UserDefaults.standard.set(kefilDurumKayitli, forKey: "kefilDurumKayitli")
    UserDefaults.standard.set(kefilSayisiKayitli, forKey: "kefilSayisiKayitli")
    UserDefaults.standard.set(teminatDurumKayitli, forKey: "teminatDurumKayitli")
    UserDefaults.standard.set(teminatMiktariKayitli, forKey: "teminatMiktariKayitli")
    UserDefaults.standard.set(tahsisDurumKayitli, forKey: "tahsisDurumKayitli")
    UserDefaults.standard.set(tahsisMiktariKayitli, forKey: "tahsisMiktariKayitli")
    UserDefaults.standard.set(komisyonDurumKayitli, forKey: "komisyonDurumKayitli")
    UserDefaults.standard.set(komisyonMiktarKayitli, forKey: "komisyonMiktarKayitli")
    UserDefaults.standard.set(ekspertizDurumKayitli, forKey: "ekspertizDurumKayitli")
    UserDefaults.standard.set(ekspertizMiktarKayitli, forKey: "ekspertizMiktarKayitli")
    UserDefaults.standard.set(tasinmazRehinDurumKayitli, forKey: "tasinmazRehinDurumKayitli")
    UserDefaults.standard.set(tasinmazRehinMiktarKayitli, forKey: "tasinmazRehinMiktarKayitli")
    UserDefaults.standard.set(aciklamaKayitli, forKey: "aciklamaKayitli")
    UserDefaults.standard.set(digerMasraflarKayitli, forKey: "digerMasraflarKayitli")
    UserDefaults.standard.set(tarih, forKey: "tarih")
}

var banka:String = "Yazılmamış"
var kefilDurum:Bool = false
var kefilSayisi:Int = 0
var teminatDurum:Bool = false
var teminatMiktari:String = "Yok"
var tahsisDurum:Bool = false
var tahsisMiktari:Double = 0.0
var komisyonDurum:Bool = false
var komisyonMiktar:Double = 0.0
var ekspertizDurum:Bool = false
var ekspertizMiktar:Double = 0.0
var tasinmazRehinDurum:Bool = false
var tasinmazRehinMiktar:Double = 0.0
var aciklama:String = "Yok"
var digerMasraflar:String = "Yok"
var bugunTarih = ""

//Userdefaults kayıtlı
var krediTuruKayitli = [Int]()
var krediTutariKayitli = [Double]()
var faizOraniKayitli = [Double]()
var vadeKayitli = [Double]()
var taksitTutariKayitli = [Double]()
var toplamOdemeKayitli = [Double]()
var toplamFaizKayitli = [Double]()

var bankaIsmiKayitli = [String]()
var aciklamaKayitli = [String]()
var digerMasraflarKayitli = [String]()
var kefilDurumKayitli = [Bool]()
var teminatDurumKayitli = [Bool]()
var kefilSayisiKayitli = [Int]()
var teminatMiktariKayitli = [String]()
var tahsisDurumKayitli = [Bool]()
var tahsisMiktariKayitli = [Double]()
var komisyonDurumKayitli = [Bool]()
var komisyonMiktarKayitli = [Double]()
var ekspertizDurumKayitli = [Bool]()
var ekspertizMiktarKayitli = [Double]()
var tasinmazRehinDurumKayitli = [Bool]()
var tasinmazRehinMiktarKayitli = [Double]()
var tarih = [String]()

class krediKaydetViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    @IBOutlet weak var krediTuru: UILabel!
    @IBOutlet weak var krediTutari: UILabel!
    @IBOutlet weak var faizOrani: UILabel!
    @IBOutlet weak var vade: UILabel!
    @IBOutlet weak var taksitTutari: UILabel!
    @IBOutlet weak var toplamOdeme: UILabel!
    @IBOutlet weak var toplamFaiz: UILabel!

    @IBOutlet weak var bankaIsmi: UITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewDigerMas: UITextView!
    
    @IBOutlet weak var kefil: UITextField!
   
    @IBOutlet weak var teminat: UITextField!
    @IBOutlet weak var vergilerDahil: UILabel!
    @IBOutlet weak var ekspertizLabel: UILabel!
    @IBOutlet weak var ekspertizLabelIkiNokta: UILabel!
    @IBOutlet weak var tasinmazRehinLabel: UILabel!
    @IBOutlet weak var tasinmazRehinLabelIkiNokta: UILabel!
    @IBOutlet weak var ekspertizSwitch: UISwitch!
    @IBOutlet weak var ekspertizTextView: UITextField!
    @IBOutlet weak var tasinmazSwitch: UISwitch!
    @IBOutlet weak var tasinmazRehinTextView: UITextField!
    @IBOutlet weak var komisyonSwitch: UISwitch!
    @IBOutlet weak var komisyonTextView: UITextField!
    @IBOutlet weak var tahsisSwitch: UISwitch!
    @IBOutlet weak var tahsisTextView: UITextField!
    @IBOutlet weak var kefilSwitch: UISwitch!
    
    @IBAction func bankaTextView(_ sender: Any) {
        if bankaIsmi.text?.characters.count != 0 {
            banka = (self.bankaIsmi.text!).uppercased()
        }
    }
    
    @IBAction func kefilVar(_ sender: Any) {
        if kefilSwitch.isOn == true{
            kefil.isHidden = false
            kefilDurum = true
        }else{
            kefilDurum = false
            
            kefil.text = "Yok"
            kefil.isHidden = true
        }
    }
    
    @IBAction func kefil(_ sender: Any) {
        if kefil.text?.characters.count != 0 {
        kefilSayisi = Int(self.kefil.text!)!
        }
    }
    
    
 
    @IBOutlet weak var teminatSwitch: UISwitch!
    @IBAction func teminatVar(_ sender: Any) {
        if teminatSwitch.isOn == true{
            teminat.isHidden = false
            teminatDurum = true
        }else{
            teminatDurum = false
            
            teminat.text = "Yok"
            teminat.isHidden = true
        }
    }

    @IBAction func teminat(_ sender: Any) {
        if teminat.text?.characters.count != 0 {
            teminatMiktari = self.teminat.text!
        }
    }
    @IBAction func tahsisSwitch(_ sender: Any) {
        if tahsisSwitch.isOn == true{
            tahsisTextView.isHidden = false
            tahsisDurum = true
        }else{
            tahsisDurum = false
            tahsisTextView.text = "Yok"
            tahsisTextView.isHidden = true
        }
    }
    @IBAction func tahsisTextView(_ sender: Any) {
        if tahsisTextView.text?.characters.count != 0 {
            tahsisMiktari = Double(self.tahsisTextView.text!)!
        }
    }
    
    
    @IBAction func komisyonSwitch(_ sender: Any) {
        if komisyonSwitch.isOn == true{
            komisyonTextView.isHidden = false
            komisyonDurum = true
        }else {
            komisyonDurum = false
            komisyonTextView.text = "Yok"
            komisyonTextView.isHidden = true
        }
    }
    @IBAction func komisyonTextView(_ sender: Any) {
        if komisyonTextView.text?.characters.count != 0 {
            komisyonMiktar = Double(self.komisyonTextView.text!)!
        }
    }
    
    
    
    @IBAction func ekspertizSwitch(_ sender: Any) {
        if ekspertizSwitch.isOn == true{
            ekspertizTextView.isHidden = false
            ekspertizDurum = true
            
        }else{
            ekspertizDurum = false
            ekspertizTextView.text = "Yok"
            ekspertizTextView.isHidden = true
        }
    }
    @IBAction func ekspertizTextView(_ sender: Any) {
        if ekspertizTextView.text?.characters.count != 0 {
            ekspertizMiktar = Double(self.ekspertizTextView.text!)!
        }
    }
    
    
    
    @IBAction func tasinmazSwitch(_ sender: Any) {
        if tasinmazSwitch.isOn == true {
            tasinmazRehinTextView.isHidden = false
            tasinmazRehinDurum = true
            
        }else {
            tasinmazRehinDurum = false
            tasinmazRehinTextView.text = "Yok"
            tasinmazRehinTextView.isHidden = true
        }
    }
    @IBAction func tasinmazTextView(_ sender: Any) {
        if tasinmazRehinTextView.text?.characters.count != 0 {
            tasinmazRehinMiktar = Double(self.tasinmazRehinTextView.text!)!
        }
    }
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        self.kefil.delegate = self
        self.teminat.delegate = self
        self.bankaIsmi.delegate = self
        self.textView.delegate = self
        self.textViewDigerMas.delegate = self
        
        switch segmentedD {
        case 0:
            krediTuru.text = "İhtiyaç"
            vergilerDahil.isHidden = false
            vergilerDahil.text = "KKDF ve BSMV Vergileri Dahil"
        case 1:
            krediTuru.text = "Taşıt"
            vergilerDahil.isHidden = false
            vergilerDahil.text = "KKDF ve BSMV Vergileri Dahil"
        case 2:
            krediTuru.text = "Konut"
            vergilerDahil.isHidden = true
            ekspertizLabel.isHidden = false
            ekspertizLabelIkiNokta.isHidden = false
            ekspertizSwitch.isHidden = false
            tasinmazSwitch.isHidden = false
            tasinmazRehinLabel.isHidden = false
            tasinmazRehinLabelIkiNokta.isHidden = false
        default:
            print("Segmented hatalı")
        }
        krediTutari.text = "\(String(Int(krediTutariD))) TL"
        faizOrani.text = String("% \(faizOraniD*100)")
        vade.text = "\(String(Int(vadeD))) Ay"
        taksitTutari.text = "\(String(sayiyiYuvarla(deger: aylikTaksitD, basamak: 2))) TL"
        toplamOdeme.text = "\(String(sayiyiYuvarla(deger: toplamOdemeD, basamak: 2))) TL"
        toplamFaiz.text = "\(String(sayiyiYuvarla(deger: toplamFaizD, basamak: 2))) TL"
        
        textView.layer.borderWidth = 1
        textView.layer.cornerRadius = 10
        textView.layer.borderColor = UIColor.black.cgColor
        textViewDigerMas.layer.borderWidth = 1
        textViewDigerMas.layer.cornerRadius = 10
        textViewDigerMas.layer.borderColor = UIColor.black.cgColor
    }
    
    @IBAction func kaydet(_ sender: Any) {
        
    
            let alert = UIAlertController(title: "Kredi Bilgileriniz Kaydedilecektir.", message: "Kaydet butonu seçildiğinde kayıt işlemi yapılıp kredilerim sayfasına yönlendirileceksiniz.", preferredStyle: UIAlertControllerStyle.actionSheet)
            let btnCancel = UIAlertAction(title: "Vazgeç", style: UIAlertActionStyle.cancel) { (alert: UIAlertAction) in
                
            }
            let btnTamam = UIAlertAction(title: "Kaydet", style: UIAlertActionStyle.default) { (alert: UIAlertAction) in
                krediTuruKayitli.append(segmentedD)
                krediTutariKayitli.append(krediTutariD)
                faizOraniKayitli.append(faizOraniD)
                vadeKayitli.append(vadeD)
                taksitTutariKayitli.append(aylikTaksitD)
                toplamOdemeKayitli.append(toplamOdemeD)
                toplamFaizKayitli.append(toplamFaizD)
                bankaIsmiKayitli.append(banka)
                kefilDurumKayitli.append(kefilDurum)
                kefilSayisiKayitli.append(kefilSayisi)
                teminatDurumKayitli.append(teminatDurum)
                teminatMiktariKayitli.append(teminatMiktari)
                tahsisDurumKayitli.append(tahsisDurum)
                tahsisMiktariKayitli.append(tahsisMiktari)
                komisyonDurumKayitli.append(komisyonDurum)
                komisyonMiktarKayitli.append(komisyonMiktar)
                ekspertizDurumKayitli.append(ekspertizDurum)
                ekspertizMiktarKayitli.append(ekspertizMiktar)
                tasinmazRehinDurumKayitli.append(tasinmazRehinDurum)
                tasinmazRehinMiktarKayitli.append(tasinmazRehinMiktar)
                aciklamaKayitli.append(aciklama)
                digerMasraflarKayitli.append(digerMasraflar)
                self.tarihTut()
                tarih.append(bugunTarih)
                
                userDefaultsKayit()
               
//                print("----------------------------------")
//                print("Kredi Turu:\(krediTuruKayitli)")
//                print("TahsisDurum:\(tahsisDurumKayitli)")
//                print("aciklama:\(aciklamaKayitli)")
//                print("digerMasraflar:\(digerMasraflarKayitli)")
                
                let form = self.storyboard?.instantiateViewController(withIdentifier: "tabBar") as! UITabBarController
                form.selectedIndex = 1
                form.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
                
                self.present(form, animated: true, completion: nil)
            }
            alert.addAction(btnCancel)
            alert.addAction(btnTamam)
            self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func sayiyiYuvarla(deger:Double, basamak:Int)->Double{
        
        let carpan = pow(10.0, Double(basamak))
        
        return (deger*carpan).rounded()/carpan
    }
    
    //klavyeyi kapat
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        bankaIsmi.resignFirstResponder()
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        textViewDigerMas.resignFirstResponder()
        return true
    }
    
    //tarih
    func tarihTut(){
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let dates = "\(day)/\(month)/\(year)"
        bugunTarih = dates
    }
}
