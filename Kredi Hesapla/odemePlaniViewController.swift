//
//  odemePlaniViewController.swift
//  Kredi Hesapla
//
//  Created by Atakan Cengiz KURT on 9.08.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit



class odemePlaniViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var baslik: UILabel!
    @IBOutlet weak var tutarOranVade: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var toplamT: UILabel!
    @IBOutlet weak var anaParaT: UILabel!
    @IBOutlet weak var faizT: UILabel!
    @IBOutlet weak var vergilerDahil: UILabel!
    
    var vadeB = Double()
    var krediTutariB = Double()
    var faizOraniB = Double()
    var aylikTaksitB = Double()
    var toplamOdemeB = Double()
    var toplamFaizB = Double()
    var segmentedB = Int()
    
    var aySayisi = [Int]()
    var faiz = [Double]()
    var anaPara = [Double]()
    var kalan = [Double]()
    var paraDusus = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tutarOranVade.text = "\(krediTutariB) TL - %\(faizOraniB*100) - \(Int(vadeB)) ay"
        
        switch segmentedB {
        case 0:
            baslik.text = "İhtiyaç Kredisi Ödeme Planı"
            faizOraniB = faizOraniB * 1.20
        case 1:
            baslik.text = "Taşıt Kredisi Ödeme Planı"
            faizOraniB = faizOraniB * 1.20
        case 2:
            baslik.text = "Konut Kredisi Ödeme Planı"
            vergilerDahil.isHidden = true
        default:
            print("segmented değeri alınamadı")
        }
        
        
        
        paraDusus = krediTutariB
        
        
        
        for i in 1...Int(vadeB){
            aySayisi.append(i)
            
            let faizDusus = paraDusus * faizOraniB
            faiz.append(faizDusus)
            let anaParaDusus = aylikTaksitB - faiz[i-1]
            anaPara.append(anaParaDusus)
            let kalanDusus = paraDusus-anaPara[i-1]
            kalan.append(kalanDusus)
            
            paraDusus = kalan[i-1]
    
            
        }
        toplamT.text = "\(String(sayiyiYuvarla(deger: toplamOdemeB, basamak: 2))) TL"
        anaParaT.text = "\(String(sayiyiYuvarla(deger: krediTutariB, basamak: 2))) TL"
        faizT.text = "\(String(sayiyiYuvarla(deger: (toplamOdemeB - krediTutariB), basamak: 2))) TL"
        
        
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return Int(vadeB)
    }
    
   
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! odemePlaniTableViewCell
        
        if indexPath.row % 2 == 1{
            cell.contentView.backgroundColor = UIColor.lightGray
        }else {
            cell.contentView.backgroundColor = UIColor.white
        }

        cell.ay.text = String(aySayisi[indexPath.row])
        cell.taksit.text = String(sayiyiYuvarla(deger: aylikTaksitB, basamak: 2))
        cell.faiz.text = String(sayiyiYuvarla(deger: faiz[indexPath.row], basamak: 2))
        cell.anaPara.text = String(sayiyiYuvarla(deger: anaPara[indexPath.row], basamak: 2))
        cell.kalan.text = String(sayiyiYuvarla(deger: kalan[indexPath.row], basamak: 2))
        
        
        return cell
    }
   
    func sayiyiYuvarla(deger:Double, basamak:Int)->Double{
        
        let carpan = pow(10.0, Double(basamak))
        
        return (deger*carpan).rounded()/carpan
    }
}
