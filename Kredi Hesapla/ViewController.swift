//
//  ViewController.swift
//  Kredi Hesapla
//
//  Created by Atakan Cengiz KURT on 8.08.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit



    

        
    

var krediTutariD = Double()
var faizOraniD = Double()
var vadeD = Double()
var aylikTaksitD = Double()
var toplamOdemeD = Double()
var toplamFaizD = Double()
var segmentedD = Int()

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var segmented: UISegmentedControl!
    @IBOutlet weak var krediTutari: UITextField!
    @IBOutlet weak var faizOrani: UITextField!
    @IBOutlet weak var vade: UITextField!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var aylikTaksit: UILabel!
    @IBOutlet weak var toplamOdeme: UILabel!
    @IBOutlet weak var toplamFaiz: UILabel!
    @IBOutlet weak var odemePlani: UIButton!
    
    
    
    @IBAction func vade(_ sender: Any) {
        
        if vade.text?.characters.count != 0 {
        slider.value = Float(Int(vade.text!)!)
        
            vadeD = Double(vade.text!)!
            
            hesap()
        
        } else {
            slider.value = 0.0
            gizle()
        }
        
    }
    @IBAction func krediTutari(_ sender: Any) {
        if krediTutari.text?.characters.count != 0{
           krediTutariD = Double(krediTutari.text!)!
       
            hesap()
        }else{
            gizle()
        }
       
        
    }
    @IBAction func faizOrani(_ sender: Any) {
        if faizOrani.text?.characters.count != 0{
            
            faizOraniD = Double((faizOrani.text?.replacingOccurrences(of: ",", with: "."))!)! / 100  //faiz oranı yazılırken telefonda virgül yazıldığı anda noktaya çevirdik.
            
            hesap()
        }else{
            gizle()
        }
    }
   
    @IBAction func odemePlani(_ sender: Any) {
        
        self.krediTutari.resignFirstResponder()
        self.faizOrani.resignFirstResponder()
        self.vade.resignFirstResponder()
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let form = segue.destination as! odemePlaniViewController
        form.aylikTaksitB = aylikTaksitD
        form.faizOraniB = faizOraniD
        form.krediTutariB = krediTutariD
        form.segmentedB = segmentedD
        form.toplamFaizB = toplamOdemeD
        form.toplamOdemeB = toplamOdemeD
        form.vadeB = vadeD
        
        
        
        
    }

    @IBAction func slider(_ sender: Any) {
        vade.text = String(Int(slider.value))
        if vade.text?.characters.count != 0 {
            
            
            vadeD = Double(vade.text!)!
            hesap()
            
        } else {
            
            gizle()
        }
        
    }
    

    @IBAction func segmented(_ sender: Any) {
        hesap()
    }


    override func viewDidLoad() {
        super.viewDidLoad()
    
//        userDefaultsBosalt()
        
        self.krediTutari.delegate = self
        self.vade.delegate = self
        self.faizOrani.delegate = self
        
    }
    
    func goster(){
        aylikTaksit.isHidden = false
        toplamOdeme.isHidden = false
        toplamFaiz.isHidden = false
        odemePlani.isHidden = false
    }
    func gizle(){
        aylikTaksit.isHidden = true
        toplamOdeme.isHidden = true
        toplamFaiz.isHidden = true
        odemePlani.isHidden = true
    }
    
    
    
    
    func hesap(){
        if krediTutari.text?.characters.count != 0 && faizOrani.text?.characters.count != 0 && vade.text?.characters.count != 0{
            
            switch segmented.selectedSegmentIndex.description {
            case "0":
                segmentedD = segmented.selectedSegmentIndex
              let faizOraniDi = faizOraniD*1.20
                
                aylikTaksitD =  krediTutariD * faizOraniDi * pow((1.0 + faizOraniDi), vadeD) / (pow((1.0 + faizOraniDi), vadeD) - 1)

                
                aylikTaksit.text = "Aylık Taksit: \(String(describing: sayiyiYuvarla(deger: aylikTaksitD, basamak: 2))) TL"
                
                toplamOdemeD = aylikTaksitD * vadeD
                toplamOdeme.text = "Toplam Ödeme: \(String(describing: sayiyiYuvarla(deger: toplamOdemeD, basamak: 2))) TL"
                toplamFaizD = (aylikTaksitD * vadeD) - krediTutariD
                toplamFaiz.text = "Toplam Faiz: \(String(describing: sayiyiYuvarla(deger: toplamFaizD, basamak: 2))) TL"
            case "1":
                segmentedD = segmented.selectedSegmentIndex
                let faizOraniDt = faizOraniD*1.20
                
                aylikTaksitD =  krediTutariD * faizOraniDt * pow((1.0 + faizOraniDt), vadeD) / (pow((1.0 + faizOraniDt), vadeD) - 1)

                
                aylikTaksit.text = "Aylık Taksit: \(String(describing: sayiyiYuvarla(deger: aylikTaksitD, basamak: 2))) TL"
                
                toplamOdemeD = aylikTaksitD * vadeD
                toplamOdeme.text = "Toplam Ödeme: \(String(describing: sayiyiYuvarla(deger: toplamOdemeD, basamak: 2))) TL"
                toplamFaizD = (aylikTaksitD * vadeD) - krediTutariD
                toplamFaiz.text = "Toplam Faiz: \(String(describing: sayiyiYuvarla(deger: toplamFaizD, basamak: 2))) TL"
            case "2":
                segmentedD = segmented.selectedSegmentIndex
                let faizOraniDk = faizOraniD
                aylikTaksitD =  krediTutariD * faizOraniDk * pow((1.0 + faizOraniDk), vadeD) / (pow((1.0 + faizOraniDk), vadeD) - 1)


                
                aylikTaksit.text = "Aylık Taksit: \(String(describing: sayiyiYuvarla(deger: aylikTaksitD, basamak: 2))) TL"
                
                toplamOdemeD = aylikTaksitD * vadeD
                toplamOdeme.text = "Toplam Ödeme: \(String(describing: sayiyiYuvarla(deger: toplamOdemeD, basamak: 2))) TL"
                toplamFaizD = (aylikTaksitD * vadeD) - krediTutariD
                toplamFaiz.text = "Toplam Faiz: \(String(describing: sayiyiYuvarla(deger: toplamFaizD, basamak: 2))) TL"
                       default:
                print("Hatalı seçim")
            }
            
            
            goster()
        }else {
            gizle()
        }
    }
    func sayiyiYuvarla(deger:Double, basamak:Int)->Double{
        
        let carpan = pow(10.0, Double(basamak))
        
        return (deger*carpan).rounded()/carpan
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
        
    }
    
    func userDefaultsBosalt (){
        krediTuruKayitli = []
        krediTutariKayitli = []
        faizOraniKayitli = []
        vadeKayitli = []
        taksitTutariKayitli = []
        toplamOdemeKayitli = []
        toplamFaizKayitli = []
        bankaIsmiKayitli = []
        aciklamaKayitli = []
        digerMasraflarKayitli = []
        kefilDurumKayitli = []
        teminatDurumKayitli = []
        kefilSayisiKayitli = []
        teminatMiktariKayitli = []
        tahsisDurumKayitli = []
        tahsisMiktariKayitli = []
        komisyonDurumKayitli = []
        komisyonMiktarKayitli = []
        ekspertizDurumKayitli = []
        ekspertizMiktarKayitli = []
        tasinmazRehinDurumKayitli = []
        tasinmazRehinMiktarKayitli = []
        tarih = []
        userDefaultsKayit()
    }
}
