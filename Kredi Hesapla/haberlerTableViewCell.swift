//
//  haberlerTableViewCell.swift
//  Kredi Hesapla
//
//  Created by Atakan Cengiz KURT on 15.08.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class haberlerTableViewCell: UITableViewCell {

    @IBOutlet weak var haberAciklama: UILabel!
    @IBOutlet weak var haberResim: UIImageView!
    @IBOutlet weak var view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.haberResim.image = UIImage(named: "money")
        // Set cell to initial state here, reset or set values
    }
}
