//
//  haberlerViewController.swift
//  Kredi Hesapla
//
//  Created by Atakan Cengiz KURT on 14.08.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import AEXML
import Alamofire
import SDWebImage
import SystemConfiguration

var haberMetni = [String]()
var haberAciklama = [String]()
var haberLink = [String]()
var haberResim = [String]()


class haberlerViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var haberlerTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isInternetAvailable() == false
        {
            alertController(title: "İNTERNET HATASI", message: "İnternet bağlantınız yok!")
        }else{
            print("internet bağlı")
        }
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Alamofire.request("http://www.trthaber.com/xml_mobile.php?tur=xml_genel&kategori=ekonomi&adet=20&selectEx=yorumSay,okunmaadedi,anasayfamanset,kategorimanset").response { (response) in
            guard let xmlData = response.data else {return}
            
            let options = AEXMLOptions()
            do{
                let xmlDoc = try AEXMLDocument(xml: xmlData, options: options)
                //                print(xmlDoc.root["haber"]["haber_aciklama"].value!)
                
                if let haberler = xmlDoc.root["haber"].all{
                    for haber in haberler{
                        
                        haberResim.append(haber["haber_resim"].string)
                        haberAciklama.append(haber["haber_aciklama"].string)
                        haberLink.append(haber["haber_link"].string)
                        haberMetni.append(haber["haber_metni"].string)
                        
                    }
                    DispatchQueue.main.async {
                        self.haberlerTableView.reloadData()
                    }
                    
                }
            }catch{
                print("Hata oluştu")
            }
            
        }
    }

   
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return haberAciklama.count
    }
    
    
   
    
   
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "haberlerCell", for: indexPath) as! haberlerTableViewCell
        
        cell.haberAciklama.text = haberAciklama[indexPath.row]
        
        let resim = SDWebImageDownloader.shared().downloadImage(with: URL(string: haberResim[indexPath.row]), options: [], progress: nil) { (image, data, error, finish) in
            DispatchQueue.main.async {
                cell.haberResim.image = image
                
            }
            
        }
        cell.contentView.backgroundColor = UIColor.lightGray
        cell.haberResim.layer.cornerRadius = 10
        cell.haberResim.layer.masksToBounds = true
        cell.view.layer.cornerRadius = 10
        
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
        
    }

    
    //internet kontrolü
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    //internet kontrolü
    
    
    func alertController(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        
        let btnCancel = UIAlertAction(title: "Tamam", style: UIAlertActionStyle.cancel) { (ACTION) in
            
        }
        alert.addAction(btnCancel)
    }
}
