//
//  kredilerimViewController.swift
//  Kredi Hesapla
//
//  Created by Atakan Cengiz KURT on 14.08.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit


func userDefaultsObject(){
    krediTuruKayitli = UserDefaults.standard.object(forKey: "krediTuruKayitli") as! [Int]
    krediTutariKayitli = UserDefaults.standard.object(forKey: "krediTutariKayitli") as! [Double]
    faizOraniKayitli = UserDefaults.standard.object(forKey: "faizOraniKayitli") as! [Double]
    vadeKayitli = UserDefaults.standard.object(forKey: "vadeKayitli") as! [Double]
    taksitTutariKayitli = UserDefaults.standard.object(forKey: "taksitTutariKayitli") as! [Double]
    toplamOdemeKayitli = UserDefaults.standard.object(forKey: "toplamOdemeKayitli") as! [Double]
    toplamFaizKayitli = UserDefaults.standard.object(forKey: "toplamFaizKayitli") as! [Double]
    bankaIsmiKayitli = UserDefaults.standard.object(forKey: "bankaIsmiKayitli") as! [String]
    kefilDurumKayitli = UserDefaults.standard.object(forKey: "kefilDurumKayitli") as! [Bool]
    kefilSayisiKayitli = UserDefaults.standard.object(forKey: "kefilSayisiKayitli") as! [Int]
    teminatDurumKayitli = UserDefaults.standard.object(forKey: "teminatDurumKayitli") as! [Bool]
    teminatMiktariKayitli = UserDefaults.standard.object(forKey: "teminatMiktariKayitli") as! [String]
    tahsisDurumKayitli = UserDefaults.standard.object(forKey: "tahsisDurumKayitli") as! [Bool]
    tahsisMiktariKayitli = UserDefaults.standard.object(forKey: "tahsisMiktariKayitli") as! [Double]
    komisyonDurumKayitli = UserDefaults.standard.object(forKey: "komisyonDurumKayitli") as! [Bool]
    komisyonMiktarKayitli = UserDefaults.standard.object(forKey: "komisyonMiktarKayitli") as! [Double]
    ekspertizDurumKayitli = UserDefaults.standard.object(forKey: "ekspertizDurumKayitli") as! [Bool]
    ekspertizMiktarKayitli = UserDefaults.standard.object(forKey: "ekspertizMiktarKayitli") as! [Double]
    tasinmazRehinDurumKayitli = UserDefaults.standard.object(forKey: "tasinmazRehinDurumKayitli") as! [Bool]
    tasinmazRehinMiktarKayitli = UserDefaults.standard.object(forKey: "tasinmazRehinMiktarKayitli") as! [Double]
    aciklamaKayitli = UserDefaults.standard.object(forKey: "aciklamaKayitli") as! [String]
    digerMasraflarKayitli = UserDefaults.standard.object(forKey: "digerMasraflarKayitli") as! [String]
    
    tarih = UserDefaults.standard.object(forKey: "tarih") as! [String]
}


class kredilerimViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        
        userDefaultsObject()
        
        // Do any additional setup after loading the view.
    }

    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return krediTuruKayitli.count
    }
    
    
  
    
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "krediCell", for: indexPath) as! kredilerimTableViewCell
        
        if indexPath.row % 2 == 1{
            cell.contentView.backgroundColor = UIColor.lightGray
        }else {
            cell.contentView.backgroundColor = UIColor.white
        }
        
        
        cell.bankaIsmi.text = bankaIsmiKayitli[indexPath.row]
        cell.krediTutari.text = "\(String(sayiyiYuvarla(deger: krediTutariKayitli[indexPath.row], basamak: 2) )) TL"
        cell.faizOrani.text = "% \(String(sayiyiYuvarla(deger: faizOraniKayitli[indexPath.row] * 100, basamak: 2) ))"
        cell.vade.text = String(Int(vadeKayitli[indexPath.row]))
        cell.taksitTutari.text = "\(String(sayiyiYuvarla(deger: taksitTutariKayitli[indexPath.row], basamak: 2) )) TL"
        cell.toplamGeriOdeme.text = "\(String(sayiyiYuvarla(deger: toplamOdemeKayitli[indexPath.row], basamak: 2) )) TL"
        switch krediTuruKayitli[indexPath.row] {
        case 0:
            cell.krediTuru.text = "İhtiyaç"
        case 1:
            cell.krediTuru.text = "Taşıt"
        case 2:
            cell.krediTuru.text = "Konut"
        default:
            print("kredi türü hata")
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
        
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete
        {
            krediTuruKayitli.remove(at: indexPath.row)
            krediTutariKayitli.remove(at: indexPath.row)
            faizOraniKayitli.remove(at: indexPath.row)
            vadeKayitli.remove(at: indexPath.row)
            taksitTutariKayitli.remove(at: indexPath.row)
            toplamOdemeKayitli.remove(at: indexPath.row)
            toplamFaizKayitli.remove(at: indexPath.row)
            
            bankaIsmiKayitli.remove(at: indexPath.row)
            aciklamaKayitli.remove(at: indexPath.row)
            digerMasraflarKayitli.remove(at: indexPath.row)
            kefilDurumKayitli.remove(at: indexPath.row)
            teminatDurumKayitli.remove(at: indexPath.row)
            kefilSayisiKayitli.remove(at: indexPath.row)
            teminatMiktariKayitli.remove(at: indexPath.row)
            tahsisDurumKayitli.remove(at: indexPath.row)
            tahsisMiktariKayitli.remove(at: indexPath.row)
            komisyonMiktarKayitli.remove(at: indexPath.row)
            komisyonDurumKayitli.remove(at: indexPath.row)
            ekspertizMiktarKayitli.remove(at: indexPath.row)
            ekspertizDurumKayitli.remove(at: indexPath.row)
           
            
            tarih.remove(at: indexPath.row)
            
            
            
            tableView.reloadData()
            userDefaultsKayit()
        }
        
        
    }
    func sayiyiYuvarla(deger:Double, basamak:Int)->Double{
        
        let carpan = pow(10.0, Double(basamak))
        
        return (deger*carpan).rounded()/carpan
    }
}
