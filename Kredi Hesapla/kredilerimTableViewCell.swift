//
//  kredilerimTableViewCell.swift
//  Kredi Hesapla
//
//  Created by Atakan Cengiz KURT on 19.08.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class kredilerimTableViewCell: UITableViewCell {

    @IBOutlet weak var bankaIsmi: UILabel!
    @IBOutlet weak var krediTuru: UILabel!
    @IBOutlet weak var krediTutari: UILabel!
    @IBOutlet weak var faizOrani: UILabel!
    @IBOutlet weak var vade: UILabel!
    @IBOutlet weak var taksitTutari: UILabel!
    @IBOutlet weak var toplamGeriOdeme: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
